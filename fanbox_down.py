#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
#         File:           fanbox_down.py
#         Contains:       Main application code for fanbox_down
#         Copyright:      (C) 2022-2025 supercell <stigma@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# -------------------------------------------------------------------------
from __future__ import print_function

import json
import os
import sys
from time import sleep

from argparse import ArgumentParser
from datetime import datetime
from datetime import timedelta
from datetime import tzinfo
from math import pow


def log(msg, section="debug"):
    with open(LOG_FILENAME, 'a') as log_file:
        if sys.version_info.major == 2:
            _msg = msg.encode("utf-8")
            print("[{}]: {}".format(section, _msg), file=log_file)
        else:
            print("[{}]: {}".format(section, msg), file=log_file)


class JST(tzinfo):
    def dst(self, dt):
        return timedelta(0)

    def tzname(self, dt):
        return 'JST'

    def utcoffset(self, dt):
        return timedelta(hours=9)


class Config:
    def __init__(self):
        self.output_dir = os.path.join(os.environ['HOME'], 'Pictures', 'fanbox')
        self.date_format = '%c'
        self.end_date = None

    def load(self, file_object):
        raw_text = file_object.read()
        json_obj = json.loads(raw_text)

        if 'outputDirectory' in json_obj:
            self.output_dir = json_obj['outputDirectory']
        if 'dateFormat' in json_obj:
            self.date_format = json_obj['dateFormat']

    def set_date_format(self, date_format):
        self.date_format = date_format

    def set_end_date(self, end_date):
        try:
            self.end_date = datetime.strptime(end_date, '%Y-%m-%d')
            self.end_date = self.end_date.replace(tzinfo=JST())
        except ValueError:
            self.end_date = None
            print('error: Failed to correctly parse provided end date.')
            print('       Must follow the format of YYYY-mm-dd.')

    def set_output_directory(self, output_directory):
        self.output_dir = output_directory


def xdg_config(subdir=None):
    """
    Return the value of XDG_CONFIG_HOME, or the fallback, plus `subdir`
    """
    from os.path import expanduser

    # Works on both POSIX and NT
    home = expanduser("~")
    cfg_dir = os.environ.get('XDG_CONFIG_HOME',
                             os.path.join(home, '.config'))

    assert cfg_dir is not None

    if subdir is None:
        return cfg_dir

    return os.path.join(cfg_dir, subdir)


def xdg_state(subdir=None, filename=None):
    """
    Return the value of XDG_STATE_HOME, or the fallback, with the
    addition of *subDir* and *fileName* if they're not `None`.
    """
    from os.path import expanduser

    # Works on both POSIX and NT
    home = expanduser("~")
    state_dir = os.environ.get("XDG_STATE_HOME", os.path.join(home, ".local", "state"))

    assert state_dir is not None

    if subdir is not None:
        state_dir = os.path.join(state_dir, subdir)

    if filename is not None:
        state_dir = os.path.join(state_dir, filename)

    return state_dir


def set_id(session_id):
    """
    Store a new FANBOXSESSID in the appropriate file.
    """
    config_directory = xdg_config('fanbox_down')
    if not os.path.exists(config_directory):
        os.makedirs(config_directory)

    id_file_path = os.path.join(config_directory, 'id')

    with open(id_file_path, 'w') as id_file:
        id_file.write(session_id)
        id_file.write('\n')
        id_file.flush()


def get_id():
    """
    Read the stored FANBOXSESSID from the appropriate file.
    Will return None if no FANBOXSESSID has been stored before.
    """
    envid = os.environ.get("FANBOX_DOWN_SESSID", "")
    if "" != envid:
        return envid

    id_file_path = os.path.join(xdg_config('fanbox_down'), 'id')

    if os.path.exists(id_file_path):
        with open(id_file_path) as id_file:
            first_line = id_file.readline()
            if "" != first_line:
                return first_line.strip()

    return None


# This really depends on the filesystem in use.
# escape _should_ only be a single character, but
# it doesn't matter.
def dirname_replace_reserved(dirname, escape='_'):
    reserved = "#<>$+%!`&*\'\"|{}?=/:\\ @"
    unreserved = dirname

    for char in reserved:
        unreserved = unreserved.replace(char, escape)

    return unreserved


def make_http_request(uri, headers=None):
    if sys.version_info[0] == 2:
        import urllib2 as urllib
    else:
        import urllib.request as urllib

    if headers is None:
        headers = {'Accept': '*/*'}

    req = urllib.Request(uri)
    if sys.version_info.major <= 2:
        for key, value in headers.iteritems():
            req.add_header(key, value)
    else:
        for key, value in headers.items():
            req.add_header(key, value)

    res = urllib.urlopen(req)
    return res


def make_fanbox_request(uri):
    return make_http_request(uri, DEFAULT_HEADERS)


def make_api_request(path):
    """
    Perform an API request to the pixiv servers
    :param path: The API path, such as `'post.info?postId=1'`
    :return: The parsed JSON object
    """
    import json

    res = make_fanbox_request("https://api.fanbox.cc/{}".format(path))

    json_res = json.loads(res.read().decode("utf-8"))
    return json_res


def fdmkdir(config, info):
    """
    Create a directory and any parent directories required.

    This will escape the directory path so that it is *safe* for various
    operating systems.

    :type info: dict
    :type config: Config
    :rtype: str
    """
    safe_subdirectory = os.path.join(
        dirname_replace_reserved(info['user']['name']),
        dirname_replace_reserved(info['title']))

    full_path = os.path.join(config.output_dir, safe_subdirectory)

    # Python2 compatibility: exists_ok isn't a parameter in os.makedirs.
    if not os.path.exists(full_path):
        os.makedirs(full_path)

    return full_path


def py2compat_datetime_strptime(datestr):
    """Parse ISO 8601 formatted strings to JST time."""
    # More specifically, python2's %z modifier doesn't support
    # the timezone being formatted as +00:00, only +0000.
    if len(datestr) < 19:
        return None

    if not (datestr[-6] == '+' and datestr[-3] == ':'):
        return None

    trimmed_datestr = datestr[:-6]
    date_obj = datetime.strptime(trimmed_datestr,
                                 "%Y-%m-%dT%H:%M:%S")
    date_obj = date_obj.replace(tzinfo=JST())
    return date_obj


def set_file_date(path, date):
    import time

    date_obj = py2compat_datetime_strptime(date)

    posix_time = time.mktime(date_obj.timetuple())
    os.utime(path, (posix_time, posix_time))


def shared_download(url, output_filename, headers=None):
    """
    Attempt to download the file in partial chunks to allow
    for resuming a download.
    """
    if sys.version_info[0] == 2:
        import urllib2 as urllib
    else:
        import urllib.request as urllib

    if headers is None:
        headers = dict()

    partial_filename = output_filename + ".part"
    open_mode = "wb"

    if os.path.exists(partial_filename):
        headers["Range"] = "bytes=%d-" % os.path.getsize(partial_filename)
        open_mode = "ab"

    req = urllib.Request(url)
    for header, value in headers.items():
        req.add_header(header, value)

    res = urllib.urlopen(req)

    if res.code == 416:
        # Invalid Range specification. Somehow?
        # Will have to write the entire file.
        res.close()
        open_mode = "wb"
        del headers["Range"]

        req = urllib.Request(url)
        for header, value in headers:
            req.add_header(header, value)

        res = urllib.urlopen(req)
    elif res.code == 200:
        # Ignored the Range request, presume the
        # entire file was returned.
        open_mode = "wb"

    download_size = 0
    range_start = -1
    if "Content-Range" in res.headers:
        log("Content-Range = %s" % res.headers["Content-Range"], "shared_download")
        content_range = res.headers["Content-Range"].split(" ")[1]
        total_size = content_range.split("/")[1]
        _ranges = content_range.split("/")[0]
        _range_start = _ranges.split("-")[0]

        if total_size != '*' and total_size != '':
            download_size = int(total_size)
        if _range_start != '':
            range_start = int(_range_start)

    if 0 == download_size and ("Content-Length" in res.headers):
        download_size = res.headers["Content-Length"]

    data = b'<default>'
    with open(partial_filename, open_mode) as image_file:
        if "ab" == open_mode:
            if range_start == -1:
                image_file.seek(0, os.SEEK_END)
            else:
                image_file.seek(range_start, os.SEEK_SET)

        bytes_read = image_file.tell()

        if "ab" == open_mode:
            print("[download] Resuming download at byte %d" % bytes_read)

        while data:
            data = res.read(1024)
            image_file.write(data)
            # Python2 FileObject.write doesn't return anything
            bytes_read += len(data)
            if download_size != 0:
                percent = (float(bytes_read) / float(download_size)) * 100
                print("\r[download]: Downloaded %.2f%%" % percent, end='')
            else:
                print("\r[download]: Downloaded %d bytes" % bytes_read, end='')

        print("")

    if "Range" in headers:
        del headers["Range"]

    os.rename(partial_filename, output_filename)
    return True


# Number of seconds in 9 hours
JST_OFFSET_SECONDS = (9 * 60 * 60)


def print_embedded_preamble(post_info, embed_file, config):
    """
    Print starting HTML and basic styling for the url_embed.html
    """
    import time

    post_title = post_info['title']
    if sys.version_info[0] == 2:
        post_title = post_title.encode('utf-8')

    date_obj = py2compat_datetime_strptime(post_info['publishedDatetime'])
    date_utc = date_obj - timedelta(seconds=JST_OFFSET_SECONDS)
    date_local = date_utc + timedelta(seconds=-time.timezone)
    post_date = date_local.strftime(config.date_format)

    post_fee = post_info['feeRequired']
    visibility_message = 'All users'
    if post_fee != 0:
        visibility_message = '{} JPY'.format(post_fee)

    has_cover_image = post_info['coverImageUrl'] is not None
    if has_cover_image:
        cover_image_extension = os.path.splitext(post_info['coverImageUrl'])[1].strip('.')
    else:
        cover_image_extension = None

    print('<!DOCTYPE html>', file=embed_file)
    print('<html>', file=embed_file)
    print('<head>', file=embed_file)
    print('<meta charset="utf-8">', file=embed_file)
    print('<title>{}</title>'.format(post_title), file=embed_file)
    print('<style type="text/css">', file=embed_file)
    print('body { font-family: sans-serif; }', file=embed_file)
    print('body { max-width: 720px; }', file=embed_file)
    print('body { margin: 0 auto; }', file=embed_file)
    print('img { display: block; }', file=embed_file)
    print('img { max-width: 100%; }', file=embed_file)
    print('img { max-height: 500px; }', file=embed_file)
    print('img { margin: 0 auto; }', file=embed_file)
    print('img#cover-image { max-height: 100%; }', file=embed_file)
    print('video { width: 100%; }', file=embed_file)
    print('iframe { border: 0; height: 100%; width: 100%; }', file=embed_file)
    print('div.iframely-responsive { height: 309px; }', file=embed_file)
    print('div.iframely-card > div { border-radius:3px; overflow:hidden; }', file=embed_file)
    print('div.iframely-card > div { box-shadow: rgba(0,0,0,0) 0px 1px 3px; }', file=embed_file)
    print('div.custom-embed > iframe.iframe-youtube { height: 309px; }', file=embed_file)
    print('</style>', file=embed_file)
    print('</head>', file=embed_file)
    print('<body>', file=embed_file)
    if has_cover_image:
        print('<img src="cover.{}" id="cover-image" />'.format(cover_image_extension),
              file=embed_file)
    print('<h1>{}</h1>'.format(post_title), file=embed_file)
    print('<p><small>{} &centerdot; {}</small></p>'.format(post_date, visibility_message),
          file=embed_file)


def handle_embedded_blocks(post_info, block_info, embed_file):
    """
    Handle embedded content in posts.

    This will print formatting of *block_info* to *embed_file*.
    """
    pass


def download_article(config, info):
    # Create directory
    print('[article]: Creating directory')
    directory = fdmkdir(config, info)

    if ('body' not in info) and ('blocks' not in info['body']):
        print('[article]: Failed: No blocks to download from.')
        return False

    if info['coverImageUrl'] is not None:
        print('[article]: Downloading article cover image')
        cover_image_extension = os.path.splitext(info['coverImageUrl'])[1].strip('.')
        cover_image_path = os.path.join(directory, 'cover.{}'.format(cover_image_extension))
        if os.path.exists(cover_image_path):
            print('[article]: Already downloaded cover image')
        else:
            success = shared_download(info['coverImageUrl'], cover_image_path, DEFAULT_HEADERS)
            if success:
                print('[article]: Downloaded cover image')
            else:
                print('[article]: Failed to download cover image.')

    # Create article text file
    print('[article]: Downloading article content')
    url_embeds_file_path = os.path.join(directory, 'url_embed.html')
    url_embeds_file = open(url_embeds_file_path, 'w')
    content_file_path = os.path.join(directory, 'content.txt')
    content_file = open(content_file_path, 'w')

    # Print the title of the post and other metadata to the
    # HTML file.
    print_embedded_preamble(info, url_embeds_file, config)

    for block in info['body']['blocks']:
        if block['type'] == 'p':
            if sys.version_info[0] == 2:
                text = block['text'].encode('utf-8')
            else:
                text = block['text']
            print(text, file=content_file)

            print('<p>', file=url_embeds_file)
            if 'styles' in block:
                bold_text = False
                styles = block['styles']

                for style in styles:
                    if style['type'] == 'bold':
                        bold_text = True
                        print('<strong>', end='', file=url_embeds_file)
                    else:
                        log('Unknown paragraph style {}'.format(style['type']),
                            'download_article')

                print(text, end='', file=url_embeds_file)

                if bold_text:
                    print('</strong>', end='', file=url_embeds_file)
            else:
                print(text, end='', file=url_embeds_file)

            print('</p>', file=url_embeds_file)

        elif block["type"] == "header":
            header = block["text"]

            if sys.version_info[0] == 2:
                header = header.encode("utf-8")

            header_length = len(header)

            print("{}\n{}".format(header, '*' * header_length), file=content_file)
            print('<h2>{}</h2>'.format(header), file=url_embeds_file)

        elif block["type"] == "file":
            print("[file:{}]".format(block["fileId"]), file=content_file)

            # Support embedding videos
            file_map = info['body']['fileMap'][block['fileId']]
            extension = file_map['extension']
            if extension == 'mp4':
                print('<video src="{}.{}" controls=""></video>'.format(file_map['id'], extension),
                      file=url_embeds_file)
            else:
                log('Unknown embed file extension: {}'.format(extension),
                    'download_article')
        elif block['type'] == 'image':
            image_id = block['imageId']
            extension = info['body']['imageMap'][image_id]['extension']
            print('[image:{}]'.format(image_id), file=content_file)
            print('<img src="{}.{}" />'.format(image_id, extension), file=url_embeds_file)
        elif block['type'] == 'url_embed':
            handle_embedded_blocks(info, block, url_embeds_file)
            embed_id = block['urlEmbedId']
            # Print embed to HTML file
            embed_map = info['body']['urlEmbedMap'][embed_id]
            if embed_map['type'] == 'fanbox.creator':
                cname = embed_map['profile']['creatorId']
                uname = embed_map['profile']['user']['name']
                # Handle UTF-8 characters
                if sys.version_info[0] == 2:
                    uname = uname.encode('utf-8')

                print('<a href="https://fanbox.cc/@{}">{}</a>'.format(cname, uname),
                      file=url_embeds_file)
            elif embed_map['type'] == 'fanbox.post':
                embedded_post = embed_map['postInfo']
                embed_title = embedded_post['title']
                embed_creator = embedded_post['creatorId']
                embed_id = embedded_post['id']
                if sys.version_info[0] == 2:
                    embed_title = embed_title.encode('utf-8')
                print('<a href="https://fanbox.cc/@{}/posts/{}">{}</a>'.format(embed_creator,
                                                                               embed_id, embed_title),
                      file=url_embeds_file)
            elif embed_map['type'] == 'html' or embed_map['type'] == 'html.card':
                # Contains raw HTML. Seems safe ;)
                print(embed_map['html'], file=url_embeds_file)
            else:
                if embed_map['type'] != 'default':
                    log("Invalid embed map type {} (post id {})".format(embed_map['type'],
                                                                        info['id']),
                        'download_article')
                    raise ValueError('Expected embed of type "default". Instead, got {}'.format(embed_map['type']))
                print('<div style="border:1px solid grey">\n', file=url_embeds_file)
                print('<p>{}</p>\n'.format(embed_map['host']), file=url_embeds_file)
                print('<p><a href="{}">{}</a></p>\n'.format(embed_map['url'], embed_map['url']),
                      file=url_embeds_file)
                print('</div>\n', file=url_embeds_file)

            # Print embedID to content.txt
            print("[embed:{}]".format(embed_id), file=content_file)

        # This seems to be the older version of 'url_embed'?
        elif block['type'] == 'embed':
            embed_id = block['embedId']
            embed_map = info['body']['embedMap'][embed_id]
            # The JSON for object provides a 'serviceProvider' and 'contentId'
            # We can't really create our own *rich* embeds, so a nice link will
            # do.
            print('<div class="custom-embed">', file=url_embeds_file)
            if embed_map['serviceProvider'] == 'twitter':
                print('<a href="https://twitter.com/twitter-api-sucks/status/{}">'.format(embed_map['contentId']),
                      file=url_embeds_file)
                print('Link to twitter.com</a>', file=url_embeds_file)
            elif embed_map['serviceProvider'] == 'youtube':
                print('<iframe class="iframe-youtube" src="https://youtube.com/embed/{}"></iframe>'.format(
                    embed_map['contentId']), file=url_embeds_file)
            print('</div>', file=url_embeds_file)
        else:
            msg = '{}:\n\t{} "{}".'.format('Failed to store article content in file',
                                           'Unsupported content type',
                                           block['type'])
            print("[article]: {}".format(msg), file=sys.stderr)
            log(msg, "article")

    print('</body>', file=url_embeds_file)
    url_embeds_file.close()
    if os.stat(url_embeds_file_path).st_size == 0:
        os.remove(url_embeds_file_path)

    content_file.close()
    set_file_date(content_file_path, info['publishedDatetime'])

    if 'imageMap' in info['body'] and 0 < len(info['body']['imageMap']):
        print('[article]: Downloading article images')
        length = len(info['body']['imageMap'])
        i = 1
        for _, image in info['body']['imageMap'].items():
            print('[article]: Downloading image {} of {}.'.format(i, length))
            file_path = os.path.join(directory,  '{}.{}'.format(image['id'], image['extension']))

            if os.path.exists(file_path):
                print("[article]: Image #%d has already been downloaded" % i)
                sleep(5)
                i += 1
                continue

            success = shared_download(image['originalUrl'], file_path, DEFAULT_HEADERS)
            if success:
                print("[article] Downloaded image %d" % i)
            set_file_date(file_path, info['publishedDatetime'])
            sleep(2)
            i += 1

    if "fileMap" in info["body"] and 0 < len(info["body"]["fileMap"]):
        print("[article]: Downloading article files")
        length = len(info["body"]["fileMap"])
        i = 1
        for _, file_ in info["body"]["fileMap"].items():
            print("[article]: Downloading file {} of {}.".format(i,
                                                                 length))
            # Could use "name", but "id" matches the content.txt file
            file_path = os.path.join(directory,
                                     "{}.{}".format(file_["id"],
                                                    file_["extension"]))

            if os.path.exists(file_path):
                print("[article]: File #%d already downloaded" % i)
                sleep(5)
                i += 1
                continue

            success = shared_download(file_["url"], file_path,
                                      DEFAULT_HEADERS)
            if success:
                print("[article] Downloaded file %d" % i)
            set_file_date(file_path, info["publishedDatetime"])
            sleep(5)
            i += 1

    return True


def download_image(config, item):
    # Make directory
    directory = fdmkdir(config, item)

    # Download Images.
    if "body" not in item:
        print("[image]: No body found for post.", file=sys.stderr)
        return False

    if len(item["body"]["images"]) <= 0:
        print("[image]: No content to download!", file=sys.stderr)
        return False

    images = item['body']['images']

    i = 1
    length = len(images)
    for image in images:
        print('[image]: Downloading image {} of {}.'.format(i, length))
        file_path = os.path.join(directory, '{}.{}'.format(image['id'],
                                                           image['extension']))

        if os.path.exists(file_path):
            print("[image]: Image #%d already downloaded" % i)
            sleep(2)
            i += 1
            continue

        success = shared_download(image["originalUrl"], file_path,
                                  DEFAULT_HEADERS)
        if success:
            print("[image]: Downloaded image %d" % i)
        set_file_date(file_path, item['publishedDatetime'])
        sleep(2)
        i += 1

    return True


def download_file(config, item):
    directory = fdmkdir(config, item)

    if sys.version_info.major == 2:
        log(u"Downloading post '{}' by {}".format(item['title'],
                                                  item['creatorId']),
            "download_file")
    else:
        log("Downloading post '{}' by {}".format(item['title'],
                                                 item['creatorId']),
            "download_file")

    if ('body' not in item) or (len(item['body']['files']) == 0):
        print('[file]: No files to download', file=sys.stderr)
        return False

    if 'text' in item['body']:
        print("[file]: Saving text to file...")
        with open(os.path.join(directory, "text.txt"), 'w') as f:
            post_text = item['body']['text']
            if sys.version_info.major == 2:
                print(post_text.encode('utf-8'), file=f)
            else:
                print(post_text, file=f)

    post_files = item['body']['files']

    numfiles = len(post_files)
    for idx, post_file in enumerate(post_files, start=1):
        log("Downloading a .{} file".format(post_file['extension']),
            "download_file")
        print("[file]: Downloading file {} of {} ({} MB)".format(idx,
                                                                 numfiles,
                                                                 post_file['size'] / pow(1000, 2)))

        path_file = os.path.join(directory,
                                 u"{}.{}".format(post_file['name'],
                                                 post_file['extension']))

        if os.path.exists(path_file):
            print("[file] File %d has already been downloaded" % idx)
            sleep(2)
            continue

        shared_download(post_file["url"], path_file, DEFAULT_HEADERS)
        set_file_date(path_file, item['publishedDatetime'])
        sleep(2)

    return True


def download_item(config, item):
    if sys.version_info[0] == 2:
        item_title = item["title"].encode("UTF-8")
    else:
        item_title = item["title"]

    print("[item]: Downloading {}".format(item_title))

    if item['isRestricted']:
        print('[item]: Skipped! Restricted content.')
        log("Skipped {} (isRestricted == True)".format(item["id"]),
            "download_item")
        return

    post_info = make_api_request('post.info?postId={}'.format(item['id']))

    if post_info['body']['type'] == 'article':
        success = download_article(config, post_info['body'])
    elif post_info['body']['type'] == 'image':
        success = download_image(config, post_info['body'])
    elif post_info["body"]["type"] == "file":
        success = download_file(config, post_info["body"])
    else:
        success = False
        log("Skipped {} -- Unsupported post type \"{}\"".format(
            post_info["body"]["id"], post_info["body"]["type"]))

        print('[item]: Failed! Unsupported post type "{}"'.format(
            post_info['body']['type']))

    if success:
        print("[item]: Completed {}".format(item_title))


def download_user(config, username):
    log("Requesting pages for {}".format(username), "download_user")

    pages_res = make_api_request('post.paginateCreator?creatorId={}'.format(
        username))

    if 'body' not in pages_res:
        print('error: no body in request for pagination (user: {})'.format(username),
              file=sys.stderr)
        sys.exit(1)

    pages = pages_res['body']
    number_of_pages = len(pages)

    if 0 == number_of_pages:
        print('warning: no pages found! not downloading anything.')
        sys.exit(0)

    index = 0
    for current_page in range(number_of_pages):
        print('[user]: Downloading page {} of {} for {}'.format(
            current_page + 1, number_of_pages, username))
        page_res = make_fanbox_request(pages[current_page])
        page_json = json.loads(page_res.read().decode("utf-8"))

        if 'body' not in page_json:
            print('error: no body in request for page {} for user {}'.format(
                current_page, username), file=sys.stderr)
            log("ERROR: No 'body' in page_res object", "download_user")
            continue

        page_items = page_json['body']

        for item in page_items:
            date_obj = py2compat_datetime_strptime(item['publishedDatetime'])
            if config.end_date and (date_obj < config.end_date):
                print("[user]: Finished downloading -- past end date.")
                return

            download_item(config, item)
            sleep(4)

        index += 1
        if number_of_pages > index:
            print('[user]: Preparing next page...')
            sleep(10)


def download_supported(config):
    print("[supported]: Fetching recent supported posts...")
    list_supported = make_api_request("post.listSupporting?limit=10")

    if "items" not in list_supported["body"]:
        print("[supported]: Error occurred: no items returned in response",
              file=sys.stderr)
        log("Failed \"items\" not in response", "download_supported")
        sys.exit(1)

    for item in list_supported["body"]["items"]:
        date_obj = py2compat_datetime_strptime(item["publishedDatetime"])
        if config.end_date and (date_obj < config.end_date):
            print("[supported]: Finished downloading -- past end date.")
            return

        download_item(config, item)
        sleep(10)

def set_default_cookie_from_file(path):
    try:
        with open(path, 'r') as cf:
            DEFAULT_HEADERS['Cookie'] = cf.readline().strip()
        
        if len(DEFAULT_HEADERS['Cookie']) == 0:
            print('Invalid cookie value in cookie file {}'.format(path))
            sys.exit(1)

    except FileNotFoundError:
        print('File does not exist: {}'.format(path))
        sys.exit(1)

#####
#
# CONSTANTS
#
#####

LOG_FILENAME = os.environ.get("FANBOX_DOWN_LOG_FILE",
                              xdg_state("fanbox_down", "fanbox_down.log"))
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:134.0) Gecko/20100101 Firefox/134.0"

DEFAULT_HEADERS = {
    "Accept": "*/*",
    "Cookie": "FANBOXSESSID={}".format(get_id()),
    "Referer": "https://www.fanbox.cc",
    "Origin": "https://www.fanbox.cc",
    "User-Agent": USER_AGENT
}


def main(args):
    parser = ArgumentParser(prog='fanbox_down')
    parser.add_argument('--cookie-file', help='File to read and use cookies from.')
    parser.add_argument('-e', '--end-date', help='Specify the end date.', metavar='DATE')
    parser.add_argument('--post', help='Download an individual post by ID.')
    parser.add_argument('--set-id', help='Update the FANBOXSESSID used.', metavar='ID')
    parser.add_argument('-u', '--user', help='Download content from a specific username.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.3.2')

    group_conf = parser.add_argument_group('configuration',
                                           "These options will override any settings in the configuration file.")
    group_conf.add_argument('--date-format', help='Customise the date formatting for generate HTML',
                            metavar='FORMAT')
    group_conf.add_argument('--output-directory', help='Output directory to create required structure',
                            metavar='DIR')
    options = parser.parse_args(args[1:])

    config = Config()
    config_file_path = os.path.join(xdg_config('fanbox_down'), 'config.json')

    if not os.path.exists(os.path.dirname(LOG_FILENAME)):
        os.makedirs(os.path.dirname(LOG_FILENAME))

    if os.path.exists(LOG_FILENAME):
        os.remove(LOG_FILENAME)

    if os.path.exists(config_file_path):
        with open(config_file_path) as config_file:
            config.load(config_file)

    # Set option overrides before doing anything else.
    if options.date_format:
        config.set_date_format(options.date_format)

    if options.output_directory:
        config.set_output_directory(options.output_directory)

    if options.set_id:
        set_id(options.set_id)
        print('Successfully changed the FANBOXSESSID!')
        DEFAULT_HEADERS["Cookie"] = "FANBOXSESSID={}".format(get_id())
        if sys.version_info[0] == 2:
            cont = raw_input('Do you want to continue downloading new posts? [yes/(no)] ')
        else:
            cont = input('Do you want to continue downloading new posts? [yes/(no)] ')

        if not (cont.lower() == 'y' or cont.lower() == 'yes'):
            sys.exit(0)

    if options.cookie_file:
        set_default_cookie_from_file(options.cookie_file)

    if options.end_date:
        config.set_end_date(options.end_date)
        if config.end_date is None:
            sys.exit(1)

    if options.user:
        download_user(config, options.user)
        sys.exit(0)

    if options.post:
        item_json = make_api_request('post.info?postId={}'.format(options.post))
        if 'body' not in item_json:
            print('error: failed to download post information for ID', options.post, file=sys.stderr)
            sys.exit(1)
        download_item(config, item_json['body'])
        sys.exit(0)

    download_supported(config)


if __name__ == '__main__':
    try:
        main(sys.argv)
    except KeyboardInterrupt:
        print("\r\nStopping fanbox_down.")
        sys.exit(0)
